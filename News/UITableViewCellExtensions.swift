//
//  UITableViewCellExtensions.swift
//  News
//
//  Created by Alexey Berbasov on 20.11.2018.
//  Copyright © 2018 Alexey Berbasov. All rights reserved.
//

import UIKit

extension UITableViewCell {

    static var identifier: String {
        return String(describing: self)
    }
}
