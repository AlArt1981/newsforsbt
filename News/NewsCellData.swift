//
//  NewsCellData.swift
//  News
//
//  Created by Alexey Berbasov on 20.11.2018.
//  Copyright © 2018 Alexey Berbasov. All rights reserved.
//

import UIKit

struct NewsCellData {

	var image: UIImage? // Обложка
	let imageURL: URL?
    let title: String   // Заголовок
    let source: String  // Источник новости
    let date: String    // Дата публикации
    let text: String?   // Первый абзац из текста
}
