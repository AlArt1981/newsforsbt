//
//  JSONParser.swift
//  News
//
//  Created by Alexey Berbasov on 23.11.2018.
//  Copyright © 2018 Alexey Berbasov. All rights reserved.
//

import Foundation


protocol JSONParsing {
    func parse(_ data: Data) throws -> [Article]
}

final class JSONParser: JSONParsing {

    func parse(_ data: Data) throws -> [Article] {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        let response = try decoder.decode(Response.self, from: data)
        return response.articles.map {
            Article(imageURL: URL(string: $0.urlToImage ?? ""),
                    title: $0.title,
                    source: $0.source.name,
                    date: $0.publishedAt,
                    text: $0.content)
        }
    }
}

private struct Response: Decodable {
    let status: String
    let totalResults: Int
    let articles: [Article]

    struct Article: Decodable {
        let source: Source
        let author: String?
        let title: String
        let description: String?
        let url: String
        let urlToImage: String?
        let publishedAt: Date
        let content: String?
    }

    struct Source: Decodable {
        let name: String
    }
}
