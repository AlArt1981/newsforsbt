//
//  Downloader.swift
//  News
//
//  Created by Alexey Berbasov on 20.11.2018.
//  Copyright © 2018 Alexey Berbasov. All rights reserved.
//

import Foundation

typealias DownloaderCompletionHandler = (Data?, Error?) -> Void

protocol Downloading {

    func getData(from url: URL, completion handler: @escaping DownloaderCompletionHandler)
}

final class Downloader: Downloading {

    var urlSession: URLSession = .shared

    func getData(from url: URL, completion handler: @escaping DownloaderCompletionHandler) {
		print("Will start \(url)")
        let task = urlSession.dataTask(with: url) { data, response, error in
            handler(data, error)
        }
        task.resume()
    }
}
