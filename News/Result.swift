//
//  Result.swift
//  News
//
//  Created by Alexey Berbasov on 17.11.2018.
//  Copyright © 2018 Alexey Berbasov. All rights reserved.
//

import Foundation

enum Result<Value> {

    case success(Value)
    case failure(Error)
}
