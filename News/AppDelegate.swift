//
//  AppDelegate.swift
//  News
//
//  Created by Alexey Berbasov on 17.11.2018.
//  Copyright © 2018 Alexey Berbasov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let newsStoryboard = UIStoryboard(name: "News", bundle: nil)
		let navigationController = newsStoryboard.instantiateInitialViewController() as? UINavigationController
		if let newsViewController = navigationController?.viewControllers.first as? NewsViewController {
			newsViewController.newsProvider = NewsProvider(downloader: Downloader(), parser: JSONParser())
			let imageProvider = ImageProvider(downloader: Downloader())
			newsViewController.dataSource = NewsDataSource(imageProvider: imageProvider)
		}

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        return true
    }
}
