//
//  ImageProvider.swift
//  News
//
//  Created by Alexey Berbasov on 20.11.2018.
//  Copyright © 2018 Alexey Berbasov. All rights reserved.
//

import UIKit

protocol ImageProviding {

	func image(for url: URL) -> UIImage?
	func loadImage(from url: URL, completion handler: @escaping (UIImage?) -> Void)
}

final class ImageProvider: ImageProviding {

    private let downloader: Downloading
	private let cache = NSCache<NSURL, UIImage>()

    init(downloader: Downloading) {
        self.downloader = downloader
    }

	func image(for url: URL) -> UIImage? {
		return cache.object(forKey: url as NSURL)
	}

	func loadImage(from url: URL, completion handler: @escaping (UIImage?) -> Void) {
        downloader.getData(from: url) { data, error in
            // Check error
            if let error = error {
				print("Cannot load image from \(url). Error: \(error)")
                handler(nil)
                return
            }
            // Check image
            if let data = data, let image = UIImage(data: data) {
				self.cache.setObject(image, forKey: url as NSURL)
                handler(image)
                return
            }
            // Wrong data - error
			print("Cannot get image from loaded data")
            handler(nil)
        }
    }
}
