//
//  NewsViewController.swift
//  News
//
//  Created by Alexey Berbasov on 17.11.2018.
//  Copyright © 2018 Alexey Berbasov. All rights reserved.
//

import UIKit

class NewsViewController: UITableViewController {

	// Dependencies
	var newsProvider: NewsProviding?
    var dataSource: NewsDataSourceProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "News"

        newsProvider?.getTopHeadlines { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let articles):
                    self.dataSource?.setArticles(articles)
                    self.tableView.reloadData()
                case .failure(let error):
                    self.displayAlertForError(error)
                }
            }
        }
    }

    private func displayAlertForError(_ error: Error) {
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default))
        present(alert, animated: true)
    }
}

// MARK: - UITableViewDataSource

extension NewsViewController {

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource?.numberOfRows ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellID = dataSource?.cellTypeForRow(at: indexPath.row) ?? ""
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        dataSource?.configureCell(cell, at: indexPath.row)
        return cell
    }
}
