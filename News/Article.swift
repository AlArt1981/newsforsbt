//
//  Article.swift
//  News
//
//  Created by Alexey Berbasov on 17.11.2018.
//  Copyright © 2018 Alexey Berbasov. All rights reserved.
//

import Foundation

struct Article {

    let imageURL: URL?  // Обложка. Есть не у всех новостей.
    let title: String   // Заголовок
    let source: String  // Источник новости
    let date: Date      // Дата публикации
    let text: String?   // Первый абзац из текста
}
