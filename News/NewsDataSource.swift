//
//  NewsDataSource.swift
//  News
//
//  Created by Alexey Berbasov on 17.11.2018.
//  Copyright © 2018 Alexey Berbasov. All rights reserved.
//

import UIKit

protocol NewsDataSourceProtocol {

    func setArticles(_ articles: [Article])
    var numberOfRows: Int { get }
    func cellTypeForRow(at index: Int) -> String
    func configureCell(_ cell: UITableViewCell, at index: Int)
}

class NewsDataSource: NewsDataSourceProtocol {

	private let imageProvider: ImageProviding

    private var cellsData: [NewsCellData] = []

    private lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .short
        return formatter
    }()

	init(imageProvider: ImageProviding) {
		self.imageProvider = imageProvider
	}

    func setArticles(_ articles: [Article]) {
        cellsData = articles.map {
			NewsCellData(image: nil,
						 imageURL: $0.imageURL,
						 title: $0.title,
						 source: $0.source,
						 date: dateFormatter.string(from: $0.date),
						 text: $0.text)
        }
    }

    var numberOfRows: Int {
        return cellsData.count
    }

    func cellTypeForRow(at index: Int) -> String {
        return NewsCell.identifier
    }

    func configureCell(_ cell: UITableViewCell, at index: Int) {
        guard index < numberOfRows,
            let newsCell = cell as? NewsCell else { return }

        var cellData = cellsData[index]

		if let imageURL = cellData.imageURL {
			if let image = imageProvider.image(for: imageURL) {
				cellData.image = image
			} else {
				imageProvider.loadImage(from: imageURL) { [weak newsCell] loadedImage in
					guard let loadedImage = loadedImage else { return }
					DispatchQueue.main.async {
						newsCell?.updateImage(loadedImage)
					}
				}
			}
		}
		newsCell.configure(with: cellData)
    }
}
