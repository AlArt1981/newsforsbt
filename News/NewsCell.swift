//
//  NewsCell.swift
//  News
//
//  Created by Alexey Berbasov on 20.11.2018.
//  Copyright © 2018 Alexey Berbasov. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        iconImageView.backgroundColor = .lightGray
    }

    func configure(with data: NewsCellData) {
        titleLabel.text = data.title
        sourceLabel.text = data.source
        dateLabel.text = data.date
        descriptionLabel.text = data.text
        iconImageView.image = data.image
    }

	func updateImage(_ image: UIImage) {
		iconImageView.image = image
	}
}
