//
//  NewsProvider.swift
//  News
//
//  Created by Alexey Berbasov on 17.11.2018.
//  Copyright © 2018 Alexey Berbasov. All rights reserved.
//

import Foundation

protocol NewsProviding {

    func getTopHeadlines(completion handler: @escaping (Result<[Article]>) -> Void)
}

final class NewsProvider: NewsProviding {

    private enum API {
        static let key = "c4b51895223b4c86a452ff5e58d1b847"
        static let topHeadlines = "https://newsapi.org/v2/top-headlines"
    }

    private let downloader: Downloading
    private let parser: JSONParsing

    init(downloader: Downloading, parser: JSONParsing) {
        self.downloader = downloader
        self.parser = parser
    }

    func getTopHeadlines(completion handler: @escaping (Result<[Article]>) -> Void) {
        var components = URLComponents(string: API.topHeadlines)
        components?.queryItems = [
            URLQueryItem(name: "apiKey", value: API.key),
            URLQueryItem(name: "country", value: "ru")
        ]
        guard let url = components?.url else {
            handler(Result.failure(ArticleProviderError.badURL))
            return
        }

        downloader.getData(from: url) { data, error in
            if let error = error {
                handler(Result.failure(error))
                return
            }
            guard let jsonData = data else {
                handler(Result.failure(ArticleProviderError.noResponseData))
                return
            }

            do {
                let articles = try self.parser.parse(jsonData)
                handler(Result.success(articles))
            } catch {
                handler(Result.failure(error))
            }
        }
    }
}

enum ArticleProviderError: Error {
    case badURL
    case noResponseData
    case badJSON
}
