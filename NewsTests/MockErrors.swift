//
//  MockErrors.swift
//  NewsTests
//
//  Created by Alexey Berbasov on 10.12.2018.
//  Copyright © 2018 Alexey Berbasov. All rights reserved.
//

import Foundation

enum MockError: Error {
    case some
}
