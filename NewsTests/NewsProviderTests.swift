//
//  NewsProviderTests.swift
//  NewsTests
//
//  Created by Alexey Berbasov on 26.11.2018.
//  Copyright © 2018 Alexey Berbasov. All rights reserved.
//

import XCTest
@testable import News

class NewsProviderTests: XCTestCase {

    func testFailureDueDownloadError() {
		// Given
		let invalidDownloader = MockDownloader.failure()
		let validParser = MockJSONParser(result: [])
		let providerUnderTest = NewsProvider(downloader: invalidDownloader, parser: validParser)
		let errorPromise = expectation(description: "Some download error is occurred")

		// When
		providerUnderTest.getTopHeadlines { result in
			if case .failure(_) = result {
				errorPromise.fulfill()
			}
		}
		wait(for: [errorPromise], timeout: 1)

		// Then
		XCTAssertEqual(invalidDownloader.callsCount, 1)
		XCTAssertEqual(validParser.callsCount, 0)
    }

	func testNoResponseData() {
		// Given
		let invalidDownloader = MockDownloader.withoutData()
		let validParser = MockJSONParser(result: [])
		let providerUnderTest = NewsProvider(downloader: invalidDownloader, parser: validParser)
		let errorPromise = expectation(description: "ArticleProviderError.noResponseData is occurred")
		var providerError: Error?

		// When
		providerUnderTest.getTopHeadlines { result in
			if case .failure(let error) = result {
				providerError = error
				errorPromise.fulfill()
			}
		}
		wait(for: [errorPromise], timeout: 1)

		// Then
		if let expectedError = providerError as? ArticleProviderError {
			XCTAssertEqual(expectedError, ArticleProviderError.noResponseData)
		}
		XCTAssertEqual(invalidDownloader.callsCount, 1)
		XCTAssertEqual(validParser.callsCount, 0)
	}

    func testSuccess() {
        // Given
        let validDownloader = MockDownloader.successful()
        let validParser = MockJSONParser(result: [])
        let providerUnderTest = NewsProvider(downloader: validDownloader, parser: validParser)
        let successPromise = expectation(description: "Array of articles are expected")

        // When
        providerUnderTest.getTopHeadlines { result in
            if case .success(_) = result {
                successPromise.fulfill()
            }
        }
        wait(for: [successPromise], timeout: 1)

        // Then
        XCTAssertEqual(validDownloader.callsCount, 1)
        XCTAssertEqual(validParser.callsCount, 1)
    }

    func testFailureDueParseError() {
        // Given
        let validDownloader = MockDownloader.successful()
        let invalidParser = MockJSONParser(error: MockError.some)
        let providerUnderTest = NewsProvider(downloader: validDownloader, parser: invalidParser)
        let errorPromise = expectation(description: "Some parse error is occurred")

        // When
        providerUnderTest.getTopHeadlines { result in
            if case .failure(_) = result {
                errorPromise.fulfill()
            }
        }
        wait(for: [errorPromise], timeout: 1)

        // Then
        XCTAssertEqual(validDownloader.callsCount, 1)
        XCTAssertEqual(invalidParser.callsCount, 1)
    }
}
