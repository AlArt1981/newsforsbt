//
//  MockDownloader.swift
//  NewsTests
//
//  Created by Alexey Berbasov on 27.11.2018.
//  Copyright © 2018 Alexey Berbasov. All rights reserved.
//

import Foundation
@testable import News

final class MockDownloader: Downloading {

	var callsCount: Int = 0
	private let data: Data?
	private let error: Error?

	init(data: Data?, error: Error?) {
		self.data = data
		self.error = error
	}

    func getData(from url: URL, completion handler: @escaping DownloaderCompletionHandler) {
		callsCount += 1
        handler(data, error)
    }
}

extension MockDownloader {

	static func successful() -> MockDownloader {
		return MockDownloader(data: Data(), error: nil)
	}

	static func failure() -> MockDownloader {
		return MockDownloader(data: nil, error: MockError.some)
	}

	static func withoutData() -> MockDownloader {
		return MockDownloader(data: nil, error: nil)
	}
}
