//
//  MockNewsProvider.swift
//  NewsTests
//
//  Created by Alexey Berbasov on 24.11.2018.
//  Copyright © 2018 Alexey Berbasov. All rights reserved.
//

import Foundation
@testable import News

final class MockNewsProvider: NewsProviding {

	func getTopHeadlines(completion handler: @escaping (Result<[Article]>) -> Void) {
		let a1 = Article(imageURL: URL(string: "https://cbsnews1.cbsistatic.com/hub/i/r/2018/11/15/6c02af4a-df33-473e-a3a0-4cf4692c798b/thumbnail/1200x630g1/180365b7dce6df89607610301745d207/gettyimages-1067916116.jpg")!,
						 title: "Trump: \"I would help Nancy Pelosi\" if she needs votes for speaker of the House",
						 source: "CBS News",
						 date: Date(),
						 text: "President Trump said Saturday morning he would help House Minority Leader Nancy Pelosi become speaker of the House in January if she is unable to garner enough votes in her caucus. Several recently elected Democrats, as well as some current members of Congres…")

		let a2 = Article(imageURL: URL(string: "https://i.guim.co.uk/img/media/f70b9ff254bcd2452a190f25f74322a3d2913eb3/0_50_3500_2101/master/3500.jpg?width=1200&height=630&quality=85&auto=format&fit=crop&overlay-align=bottom%2Cleft&overlay-width=100p&overlay-base64=L2ltZy9zdGF0aWMvb3ZlcmxheXMvdGctZGVmYXVsdC5wbmc&s=01ddca944326e9b2522f2b0cbf26f0ea")!,
						 title: "Several arrested as thousands block London bridges in climate rebellion",
						 source: "The Guardian (AU)",
						 date: Date(),
						 text: "More than 50 people have been arrested as thousands of demonstrators occupied five bridges in central London to voice their concern over the looming climate crisis. Protesters including families and pensioners began massing on five of London’s main bridges fr… [+6455 chars]")

		handler(Result.success([a1, a2]))
	}
}
