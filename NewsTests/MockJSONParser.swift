//
//  MockJSONParser.swift
//  NewsTests
//
//  Created by Alexey Berbasov on 27.11.2018.
//  Copyright © 2018 Alexey Berbasov. All rights reserved.
//

import Foundation
@testable import News

final class MockJSONParser: JSONParsing {

	var callsCount: Int = 0
	private let result: [Article]
	private let error: Error?

	init(result: [Article]) {
		self.result = result
		self.error = nil
	}

	init(error: Error) {
		self.result = []
		self.error = error
	}

    func parse(_ data: Data) throws -> [Article] {
		callsCount += 1
		if let error = error { throw error }
        return result
    }
}
